<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AppModule as AppModuleModel;
use Illuminate\Support\Facades\Hash;

class AppModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AppModuleModel::create([
            'code'=>'Inv',
            'name'=>'Inventory-Module',
            'active'=>true
        ]);
        AppModuleModel::create([
            'code'=>'Acc',
            'name'=>'Accounts-Module',
            'active'=>true
        ]);
        AppModuleModel::create([
            'code'=>'Pay',
            'name'=>'Payroll-Module',
            'active'=>true
        ]);
        AppModuleModel::create([
            'code'=>'Aud',
            'name'=>'Audit-Module',
            'active'=>true
        ]);
    }
}
