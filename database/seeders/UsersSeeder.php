<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User as UserModel;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserModel::create([
            'name'=>'Super Admin',
            'email'=>'super_admin@erp1.lk',
            'password'=>Hash::make('Admin#1@a')
        ]);
        UserModel::create([
            'name'=>'Damitha Wijetunga',
            'email'=>'wijetunga.damitha@gmail.com',
            'password'=>Hash::make('Damitha#1@a')
        ]);
        UserModel::create([
            'name'=>'Nishantha',
            'email'=>'nishantha@erp1.lk',
            'password'=>Hash::make('Nishanth#1@a')
        ]);
        UserModel::create([
            'name'=>'Miranga',
            'email'=>'miranga@erp1.lk',
            'password'=>Hash::make('Miranga#1@a')
        ]);
        UserModel::create([
            'name'=>'Dixon',
            'email'=>'dixon@erp1.lk',
            'password'=>Hash::make('Dixon#1@a')
        ]);
    }
}
