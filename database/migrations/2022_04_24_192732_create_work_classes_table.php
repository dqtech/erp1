<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_classes', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('app_module_id')->index();
            $table->foreign('app_module_id')->references('id')->on('app_module')->onDelete('cascade')->onUpdate('cascade');
            $table->string('code',10)->index();
            $table->string('name',100)->index();
            $table->string('start',5)->index();
            $table->string('end',5)->index();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_classes');
    }
}
