<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Home as HomeController;
use App\Http\Controllers\UPM_Module\CompanyDetails as CompanyDetailsController;
use App\Http\Controllers\UPM_Module\Branches as BranchesController;
use App\Http\Controllers\UPM_Module\WorkClasses as WorkClassesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/jquery-ui-theme-changed',[HomeController::class,'jQueryUIThemeChanged']);

Route::post('/tokens/create', function (Request $request) {
    $token = $request->user()->createToken($request->token_name);
 
    return ['token' => $token->plainTextToken];
});


Route::middleware(['auth'])->group(function(){
    Route::get('/dashboard',function(){
        return view('dashboard');
    })->name('dashboard');
    Route::get('/long-page',[HomeController::class,'LongPage'])->name('long_page');
    Route::get('/short-page',[HomeController::class,'ShortPage'])->name('short_page');
    Route::get('/jquery-ui-theme-changer-demo',[HomeController::class,'jQueryUIThemeChangerDemo'])->name('jQueryUIThemeChangerDemo');
    Route::get('/company-details',[CompanyDetailsController::class,'index'])->name('company_details_page');
    Route::post('/save-company-details',[CompanyDetailsController::class,'save'])->name('save-company-details');
    Route::get('/get-company-details',[CompanyDetailsController::class,'getCompanyDetails'])->name('get-company-details');
    Route::get('/whydo-demo',[HomeController::class,'WhydoDemo'])->name('whydo_demo');
    Route::get('/admin-lte-demo',[HomeController::class,'AdminLteDemo'])->name('admin-lte-demo');
    Route::get('/branches',[BranchesController::class,'index'])->name('branches');
    Route::post('/save-branch',[BranchesController::class,'save'])->name('save-branch');
    Route::get('/get-branch-details',[BranchesController::class,'getBranchDetails'])->name('get-branch-details');
    Route::get('/create-new-branch',[BranchesController::class,'createNewBranch'])->name('create-new-branch');
    Route::get('/edit-branch',[BranchesController::class,'editBranch'])->name('edit-branch');
    Route::get('/view-branch',[BranchesController::class,'viewBranch'])->name('view-branch');
    Route::get('/delete-branch',[BranchesController::class,'deleteBranch'])->name('delete-branch');
    Route::post('/branch-email-exists',[BranchesController::class,'emailExists'])->name('branch-email-exists');
    Route::get('/work-classes',[WorkClassesController::class,'index'])->name('work-classes');
    Route::get('/create-new-work-class',[WorkClassesController::class,'createNew'])->name('create-new-work-class');
    Route::get('/edit-work-class',[WorkClassesController::class,'edit'])->name('edit-work-class');
    Route::get('/view-work-class',[WorkClassesController::class,'view'])->name('view-work-class');
    Route::get('/delete-work-class',[WorkClassesController::class,'delete'])->name('delete-work-class');
    Route::post('/save-work-class',[WorkClassesController::class,'save'])->name('save-work-class');
    Route::post('/delete-work-class-actual',[WorkClassesController::class,'deleteAction'])->name('delete-work-class-actual');
    
    Route::post('/work-class-code-exists',[WorkClassesController::class,'codeExists'])->name('work-class-code-exists');
    Route::post('/work-class-name-exists',[WorkClassesController::class,'nameExists'])->name('work-class-name-exists');
    
    Route::post('/branch-code-exists',[BranchesController::class,'codeExists'])->name('branch-code-exists');
    Route::post('/branch-name-exists',[BranchesController::class,'nameExists'])->name('branch-name-exists');
});
    


require __DIR__.'/auth.php';
