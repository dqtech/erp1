<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;
    protected $table = 'branches';
    protected $fillable=[
        'code',
        'name',
        'address',
        'phone1',
        'phone2',
        'fax',
        'email',
        'active'
    ];
}
