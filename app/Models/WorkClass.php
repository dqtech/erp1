<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkClass extends Model
{
    use HasFactory;
    protected $table = 'work_classes';
    protected $fillable=[
        'code',
        'name',
        'app_module_id',
        'start',
        'end',
        'active'
    ];
}
