<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyDetails extends Model
{
    use HasFactory;
    protected $table = 'company_details';
    protected $fillable=[
        'name',
        'address',
        'vat_reg_no',
        'vat_percentage',
        'phone',
        'fax',
        'email',
        'active'
    ];
    
}
