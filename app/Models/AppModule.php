<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppModule extends Model
{
    use HasFactory;
    protected $table = 'app_modules';
    protected $fillable=[
        'name',
        'active'
    ];
    public function scopeActiveAlphabeticalOrder($query)
    {
        return $query->where('active', 1)->orderBy('name','asc');
    }
}
