<?php

namespace App\Http\Controllers\UPM_Module;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\CompanyDetails as CompanyDetailsModel;

class CompanyDetails extends Controller
{
    public function index(Request $request){
        
        $count = CompanyDetailsModel::count();
        
        if($count==1){
            $companyDetails=CompanyDetailsModel::find(1);
            return view('UPM_Module.company_details')->with('companyDetails',$companyDetails);
        }
        
        return view('UPM_Module.company_details')->with('companyDetails',null);
    }
    
    public function save(Request $request){
        
        $fields = $request->validate([
            'name'=>'required|min:3|max:150',
            'address'=>'required|min:10|max:500',
            'vat_reg_no'=>'',
            'vat_percentage'=>'',
            'phone'=>'',
            'fax'=>'',
            'email'=>'max:150',
            'active'=>''
        ]);
        
        $count = CompanyDetailsModel::count();
        
        if($count==0||$count==1){
            if($count==0){
                $fields['active']=$fields['active']=='on'?true:false;
                $companyDetails=CompanyDetailsModel::create($fields);
            }else if($count==1){
                $companyDetails=CompanyDetailsModel::all()->first();
                $fields['active']=$fields['active']=='on'?true:false;
                $companyDetails->update($fields);
            }
            return "<script>window.parent.window.displaySuccess();</script>";
        }
       
    }
    
    public function getCompanyDetails(Request $request){
        
        $count = CompanyDetailsModel::count();
        
        if($count==0){
            return "";
        }else{
            $companyDetails=CompanyDetailsModel::all()->first();
            return json_encode($companyDetails,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        }
            
    }
}
