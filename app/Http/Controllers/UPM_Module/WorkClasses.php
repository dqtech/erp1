<?php

namespace App\Http\Controllers\UPM_Module;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WorkClass as WorkClassModel;
use App\Models\AppModule as AppModuleModel;

class WorkClasses extends Controller
{
    //
    public function index(Request $request){
        
        $workClasses=WorkClassModel::all();
        
        $workClassesModified=array();
        
        foreach($workClasses as $workClass){
            $appModule=AppModuleModel::find($workClass->app_module_id);
        
            array_push($workClassesModified,array('id'=>$workClass->id,'app_module'=>$appModule->name,'code'=>$workClass->code,'name'=>$workClass->name,'start'=>$workClass->start,'end'=>$workClass->end,'active'=>(($workClass->active)?'Active':'In-Active')));
        }
        
        return view('UPM_Module.work-classes')->with('workClasses',$workClassesModified);
    }
    
    public function createNew(Request $request){
        
        
        
        return view('UPM_Module.create-new-work-class');
    }
    
    public function view(Request $request){
        
        $id=$request->get('id');
        
        $workClass= WorkClassModel::find($id);
        
        return view('UPM_Module.view-work-class')->with('workClass',$workClass);
    }
    
    public function edit(Request $request){
        
        $id=$request->get('id');
        
        $workClass= WorkClassModel::find($id);
        
        return view('UPM_Module.edit-work-class')->with('workClass',$workClass);
    }
    
    public function delete(Request $request){
        
        $id=$request->get('id');
        
        $workClass= WorkClassModel::find($id);
        
        return view('UPM_Module.delete-work-class')->with('workClass',$workClass);
    }
    
    public function deleteAction(Request $request){
        
        $id=$request->get('id');
        
        $workClass= WorkClassModel::find($id);
        
        $workClass->delete();
        
        return "<script>window.parent.window.displaySuccess();</script>";
    }
    
    public function save(Request $request){
        
        $fields = $request->validate([
            'id'=>'',
            'app_module_id'=>'required|min:1',
            'code'=>'required|min:1|max:10',
            'name'=>'required|min:3|max:100',
            'start'=>'required|min:5|max:5',
            'end'=>'required|min:5|max:5',
            'active'=>''
        ]);
        
        $hidden=$request->input('id');
        
        if($hidden==0){
            if(isset($fields['active'])){
                $fields['active']=$fields['active']=='on'?true:false;
            }else{
                $fields['active']=false;
            }
            $workClass= WorkClassModel::create($fields);
        }else if($hidden>0){
            $workClass=WorkClassModel::find($hidden);
            if(isset($fields['active'])){
                $fields['active']=$fields['active']=='on'?true:false;
            }else{
                $fields['active']=false;
            }
            $workClass->update($fields);
        }
        return "<script>window.parent.window.displaySuccess();</script>";
       
    }
    
    public function codeExists(Request $request){
        
        $appModuleID=$request->json('app_module_id');
        $code=$request->json('code');
        
        $count= WorkClassModel::where([['code','=',$code],['app_module_id','=',$appModuleID]])->count();
        
        if($count==0){
            return "No";
        }else{
            return "Yes";
        }
    }
    
    public function nameExists(Request $request){
        
        $appModuleID=$request->json('app_module_id');
        $name=$request->json('name');
        
        $count= WorkClassModel::where([['name','=',$name],['app_module_id','=',$appModuleID]])->count();
        
        if($count==0){
            return "No";
        }else{
            return "Yes";
        }
    }
}
