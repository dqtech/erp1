<?php

namespace App\Http\Controllers\UPM_Module;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch as BranchModel;

class Branches extends Controller
{
    //
    public function index(Request $request){
        
        $branches= BranchModel::all();
        
        return view('UPM_Module.branches')->with('branches',$branches);
    }
    
    public function viewBranch(Request $request){
        
        $id=$request->get('id');
        
        $branch= BranchModel::find($id);
        
        return view('UPM_Module.view-branch')->with('branch',$branch);
    }
    
    
    
    public function save(Request $request){
        
        $fields = $request->validate([
            'id'=>'',
            'code'=>'required|min:1|max:10',
            'name'=>'required|min:3|max:150',
            'address'=>'required|min:10|max:500',
            'phone1'=>'',
            'phone2'=>'',
            'fax'=>'',
            'email'=>'',
            'active'=>''
        ]);
        
        $hidden=$request->input('id');
        
        if($hidden==0){
            if(isset($fields['active'])){
                $fields['active']=$fields['active']=='on'?true:false;
            }else{
                $fields['active']=false;
            }
            $branch= BranchModel::create($fields);
        }else if($hidden>0){
            $branch=BranchModel::find($hidden);
            if(isset($fields['active'])){
                $fields['active']=$fields['active']=='on'?true:false;
            }else{
                $fields['active']=false;
            }
            $branch->update($fields);
        }
        return "<script>window.parent.window.displaySuccess();</script>";
       
    }
    
    public function getBranchDetails($id, Request $request){
        
        $hidden=$request->input('id');
        
        if($count==0){
            return "";
        }else{
            $branchDetails= BranchModel::find($id);
            return json_encode($branchDetails,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        }
            
    }
    
    public function createNewBranch(Request $request){
        
        return view('UPM_Module.create-new-branch');
    }
    
    public function editBranch(Request $request){
        
        $id=$request->get('id');
        
        $branch= BranchModel::find($id);
        
        return view('UPM_Module.edit-branch')->with('branch',$branch);
    }
    
    public function deleteBranch(Request $request){
        
        $id=$request->get('id');
        
        $branch= BranchModel::find($id);
        
        return view('UPM_Module.delete-branch')->with('branch',$branch);
    }
    
    public function emailExists(Request $request){
        
        $email=$request->json('email');
        
        $count= BranchModel::where('email','=',$email)->count();
        
        if($count==0){
            return "No";
        }else{
            return "Yes";
        }
    }
    
    public function codeExists(Request $request){
        
        $code=$request->json('code');
        
        $count= BranchModel::where([['code','=',$code]])->count();
        
        if($count==0){
            return "No";
        }else{
            return "Yes";
        }
    }
    
    public function nameExists(Request $request){
        
        $name=$request->json('name');
        
        $count= BranchModel::where([['name','=',$name]])->count();
        
        if($count==0){
            return "No";
        }else{
            return "Yes";
        }
    }
    
}
