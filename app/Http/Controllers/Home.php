<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class Home extends Controller
{
    //
    
    public function jQueryUIThemeChanged(Request $request){
        
        $jQueryUITheme = $request->post('jQueryUITheme');
        Cookie::forget('jQueryUITheme');
        Cookie::queue('jQueryUITheme', $jQueryUITheme, 60*24*365);
                
        return $jQueryUITheme;
    }
    
    public function LongPage(Request $request){
        return view('long_page');
    }
    
    public function ShortPage(Request $request){
        return view('short_page');
    }
    
    public function jQueryUIThemeChangerDemo(Request $request){
        return view('jquery-theme-changer-demo');
    }
    
    public function WhydoDemo(Request $request){
        return view('whydo-demo');
    }
    
    public function AdminLteDemo(Request $request){
        return view('admin-lte-demo');
    }
}
