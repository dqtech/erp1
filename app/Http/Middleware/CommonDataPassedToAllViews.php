<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\AppModule as AppModuleModel;
use Illuminate\Support\Facades\View;

class CommonDataPassedToAllViews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $app_modules = AppModuleModel::activeAlphabeticalOrder()->get();

        // Sharing is caring
        View::share('appModules', $app_modules);
        
        return $next($request);
    }
}
