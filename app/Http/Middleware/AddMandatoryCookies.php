<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class AddMandatoryCookies
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $jQueryUITheme = $request->cookie('jQueryUITheme');
        if ( ! $jQueryUITheme || $jQueryUITheme=='') {
            Cookie::queue('jQueryUITheme', 'UI-Darkness', 60*24*365);
        }
        $Language = $request->cookie('Language');
        if ( ! $Language || $Language=='') {
            Cookie::queue('Language', 'en', 60*24*365);
        }
        return $next($request);
    }
}
