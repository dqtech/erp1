<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'email' => 'E-Mail',
    'password' => 'Password', 
    'remember-me' => 'Remember Me',
    'forgot-password' => 'Forgot Password ?',
    'log-in'=>'Login',
    'dashboard' => 'Dashboard',
    'register'=>'Register',
    'you-are-logged-in'=>'You\'re logged in !',
    'log-out'=>'Log-Out',
    'jQueryUITheme'=>'jQuery UI Theme',
    'upm-module'=>'UPM - Module',
    'inventory-module'=>'Inventory-Module',
    'accounts-module'=>'Accounts-Module',
    'company-details'=>'Company-Details',
    'company-name'=>'Company-Name',
    'address'=>'Address',
    'vat-registration-no'=>'VAT Reg. No.',
    'vat-percentage'=>'VAT Percentage %',
    'phone'=>'Phone',
    'fax'=>'Fax',
    'active'=>'Active',
    'save'=>'Save',
    'refresh'=>'Refresh',
    'successfully-saved'=>'Successfully Saved.',
    'ok'=>'OK',
    'logout'=>'Logout',
    'branch-code'=>'Branch-Code',
    'branch-name'=>'Branch-Name',
    'upm-module'>'UPM-Module',
    'inventory-module'=>'Inventory-Module',
    'accounts-module'=>'Accounts-Module',
    'branches'=>'Branches',
    'email-format-invalid'=>'E-Mail format in-valid !',
    'search'=>'Search',
    'update-selected-branch'=>'Update Selected Branch',
    'create-new-branch'=>'Create New Branch',
    'new'=>'New',
    'code'=>'Code',
    'view-branch'=>'View-Branch',
    'edit-branch'=>'Edit-Branch',
    'delete-branch'=>'Delete-Branch',
    'back-to-list'=>'Back-to-List',
    'email-exists'=>'E-Mail Exists !',
    'Logout'=>'Logout',
    'UserDetails'=>'Details',
    'AreYouSure'=>'Are You Sure',
    'Yes'=>'Yes',
    'No'=>'No',
    'WorkClass'=>'Work-Class',
    'WorkClasses'=>'Work-Classes',
    'payroll-module'=>'Payroll-Module',
    'audit-module'=>'Audit-Module',
    'code'=>'Code',
    'start'=>'Start',
    'end'=>'End',
    'create-new-work-class'=>'Create-New-Work-Class',
    'view-work-class'=>'View-Work-Class',
    'edit-work-class'=>'Edit-Work-Class',
    'delete-work-class'=>'Delete-Work-Class',
    'app-module'=>'App-Module',
    'time'=>'Time',
    'delete'=>'Delete',
    'deleted'=>'Successfully-Deleted.',
    'exists'=>'Exists !',
    'required'=>'Required !'
    
    


];
