<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/css/app.css?v=<?php echo filemtime(base_path().'/resources/css/app.css') ?>" />
        
        <?php 
            $jQueryUITheme=request()->cookie('jQueryUITheme');
        ?>
        
        <link id="jQueryUIThemeCss" href="<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/<?php echo $jQueryUITheme ?>/jquery-ui.min.css" rel="stylesheet"/>
<!--        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">-->
            <link href="<?php echo env('APP_URL') ?>/resources/Bootstrap/bootstrap-5.0.2-dist/bootstrap-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo env('APP_URL') ?>/resources/Bootstrap/bootstrap-5.0.2-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!--        <link href="<?php echo env('APP_URL') ?>/resources/Whydo/whydo.css" rel="stylesheet" type="text/css"/>-->
        <script src="{{ asset('js/app.js') }}" defer></script>
        
    </head>
    <body>
        <div id="FixedToolbar">
            <div class="VeryLargeScreens">
                <div class="Toolbar">
                    <i onclick="ToggleLeftMenu(this);" class="bi bi-arrow-left Icon"></i>
                    @include('components.Logout')
                    @include('components.jQueryUITheme') 
                </div>
                
            </div>
            <div class="MediumScreens">
                <div class="Toolbar">
                    <i onclick="ToggleLeftMenu(this);" class="bi bi-arrow-left Icon"></i>
                    @include('components.Logout')
                </div>
                
            </div>
            <div class="SmallScreens">
                <div class="Toolbar">
                    <i onclick="ToggleLeftMenu(this);" class="bi bi-arrow-left Icon"></i>
                    @include('components.Logout')
                </div>

            </div>
            <div class="TinyScreens">
                <div class="Toolbar">
                    <i onclick="ToggleLeftMenu(this);" class="bi bi-arrow-left Icon"></i>
                    @include('components.Logout')
                </div>
                
            </div>
            
        </div>
        <div id="Scrollable">
            <div class="MediumScreens">
                <div class="Toolbar">
                    @include('components.jQueryUITheme')
                    
                </div>
                
            </div>
            <div class="SmallScreens">
                <div class="Toolbar">
                    @include('components.jQueryUITheme')
                    
                </div>
                <div class="Toolbar">
                    
                </div>
               
            </div>
            <div class="TinyScreens">
                <div class="Toolbar">
                    @include('components.jQueryUITheme')
                </div>
                <div class="Toolbar">
                
                </div>
                <div class="Toolbar">
                
                </div>
                
            </div>
            <div id="PageContent">
                @yield('page_content')
            </div>
        </div>
        <div id="LeftMainMenu">
            <div id="MenuItemsContainer" style="position: relative;" >
                @include('components.LeftMainMenu')
            </div>
            
        </div>
        <div id="Footer">
                
        </div>
        <script src="<?php echo env('APP_URL') ?>/resources/jQuery/jquery-3.6.0.min.js"></script>
        <script  id="jQueryUIThemeJs" src="<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/<?php echo $jQueryUITheme ?>/jquery-ui.min.js"></script>
        <script src="<?php echo env('APP_URL') ?>/resources/Bootstrap/bootstrap-5.0.2-dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo env('APP_URL') ?>/resources/jQuery/jquery.alphanum-master/jquery.alphanum.js" type="text/javascript"></script>
<!--        <script src="<?php echo env('APP_URL') ?>/resources/Whydo/whydo.jszip.min.js" type="text/javascript"></script>
        <script src="<?php echo env('APP_URL') ?>/resources/Whydo/whydo.all.min.js" type="text/javascript"></script>-->
        @yield('page_scripts_before')
        <script>
            var LeftMainMenuIsOpen=false;
            var LeftMenuIsLongerThanTheScrollable=false;
            var jQueryUITheme="<?php echo $jQueryUITheme ?>";
            $(function(){
                DisplayPage();
                BrowserWindowResized();
                setTimeout(function(){
                    BrowserWindowResized();
                    
                },100);
                window.onresize=function(){
                    setTimeout(function(){
                        BrowserWindowResized();
                    },100);
                };
                window.onscroll=function(){
                    if(LeftMenuIsLongerThanTheScrollable){
                        $('#MenuItemsContainer').css('marginTop',(-1*$(this).scrollTop())+'px');
                    }
                };
                $('.LeftMenuAccordion').accordion();
                AfterjQueryLoad();
                if(window.localStorage.getItem('LeftMainMenuIsOpen')=='Yes'){
                    ToggleLeftMenu(null);
                }
                $("#Scrollable input").focus(function(){
                    if($('.Icon').hasClass('bi-arrow-left')){
                        ToggleLeftMenu(null);
                    }
                });
            });
            function DisplayPage(){
                $('body').css('display','unset');
            }
            function ToggleLeftMenu(This){
               if($('.Icon').hasClass('bi-arrow-right')){
                   
                   $('#LeftMainMenu').css('display','unset');
                   $('#LeftMainMenu').animate({
                        width: '274px'
                   },'fast',function(){
                        $('.Icon').removeClass('bi-arrow-right');
                        $('.Icon').addClass('bi-arrow-left');
                        setTimeout(function(){
                            AdjustLeftMainMenuOrScrollableAreaHeight();
                        },100);
                        
                   });
                   LeftMainMenuIsOpen=true;
                   window.localStorage.setItem('LeftMainMenuIsOpen','Yes');
               }else if($('.Icon').hasClass('bi-arrow-left')){
                   
                   $('#LeftMainMenu').animate({
                        width: '0'
                   },'fast', function(){
                        $('#LeftMainMenu').css('display','none');
                        $('.Icon').removeClass('bi-arrow-left');
                        $('.Icon').addClass('bi-arrow-right');
                        AdjustLeftMainMenuOrScrollableAreaHeight();
                   });
                   LeftMainMenuIsOpen=false;
                   window.localStorage.setItem('LeftMainMenuIsOpen','No');
               }
            }
            function AdjustLeftMainMenuOrScrollableAreaHeight(){
                var LeftMenuHeight=$('#MenuItemsContainer').css('height');
                var ScrollableHeight=$('#Scrollable').css('height');
                LeftMenuHeight=''+LeftMenuHeight;
                LeftMenuHeight=LeftMenuHeight.replace('px','');
                LeftMenuHeight=1*LeftMenuHeight;
                ScrollableHeight=''+ScrollableHeight;
                ScrollableHeight=ScrollableHeight.replace('px','');
                ScrollableHeight=1*ScrollableHeight;
                
                if(LeftMenuHeight>ScrollableHeight&&window.innerHeight<LeftMenuHeight){
                    LeftMenuIsLongerThanTheScrollable=true;
                    let MarginBottom=LeftMenuHeight-ScrollableHeight+120;
                    $('#Scrollable').css('marginBottom',MarginBottom+'px');
                }else if(ScrollableHeight>LeftMenuHeight&&window.innerHeight<ScrollableHeight){
                    LeftMenuIsLongerThanTheScrollable=false;
                    ScrollableHeight=Math.ceil(ScrollableHeight);
                    $('#LeftMainMenu').css('minHeight',ScrollableHeight+'px');

                }else if(window.innerHeight>LeftMenuHeight){
                    $('#LeftMainMenu').css('minHeight',(window.innerHeight-120)+'px'); 
                }
            }
            function BrowserWindowResized(){
                AdjustLeftMainMenuOrScrollableAreaHeight();
            }
            
            function GotojQueryUIChangePage(){
                window.location='<?php echo env('APP_URL') ?>/public/jquery-ui-theme-changer-demo';
            }
            function Logout(){
                $.ajax({
                    url:'<?php echo env('APP_URL') ?>/public/logout',
                    type:'post',
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    data:JSON.stringify({ "_token": "<?php echo csrf_token() ?>" }),
                    success:function(response){
                        let Res=$.trim(response);
                        if(Res=='Done'){
                            window.location="<?php echo env('APP_URL') ?>/public";
                        }
                    }
                });
            }
        </script>
        @yield('page_scripts_after')
    </body>
</html>
