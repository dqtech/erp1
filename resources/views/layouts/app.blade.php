<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        
        <?php 
        

        
            $jQueryUITheme=request()->cookie('jQueryUITheme');
            
        
        ?>
        
        <link id="jQueryUIThemeCss" href="<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/<?php echo $jQueryUITheme ?>/jquery-ui.min.css" rel="stylesheet"/>
        <link href="<?php echo env('APP_URL') ?>/resources/Bootstrap/bootstrap-5.0.2-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

<!--             Page Heading 
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>-->

            <!-- Page Content -->
            <main>
                {{ $slot }}
                <table><tr><td>
                    <label>{{  __('all.jQueryUITheme') }}</label>
                    <select id="cboJQueryUITheme" onchange="jQueryThemeChanged(this);">
                        <option value="Base">Base</option>
                        <option value="Black-Tie">Black-Tie</option>
                        <option value="Blitzer">Blitzer</option>
                        <option value="Cupertino">Cupertino</option>
                        <option value="Dark-Hive">Dark-Hive</option>
                        <option value="Dot-Luv">Dot-Luv</option>
                        <option value="Eggplant">Eggplant</option>
                        <option value="Excite-Bike">Excite-Bike</option>
                        <option value="Flick">Flick</option>
                        <option value="Hot-Sneaks">Hot-Sneaks</option>
                        <option value="Humanity">Humanity</option>
                        <option value="Le-Frog">Le-Frog</option>
                        <option value="Mint-Choc">Mint-Choc</option>
                        <option value="Overcast">Overcast</option>
                        <option value="Pepper-Grinder">Pepper-Grinder</option>
                        <option value="Redmond">Redmond</option>
                        <option value="Smoothness">Smoothness</option>
                        <option value="South-Street">South-Street</option>
                        <option value="Start">Start</option>
                        <option value="Sunny">Sunny</option>
                        <option value="Swanky-Purse">Swanky-Purse</option>
                        <option value="Trontastic">Trontastic</option>
                        <option value="UI-Darkness">UI-Darkness</option>
                        <option value="UI-Lightness">UI-Lightness</option>
                        <option value="Vader">Vader</option>
                    </select>
        </td></tr></table>
                <input type="text" id="txtDp"/>
            </main>
        </div>
        <script src="<?php echo env('APP_URL') ?>/resources/jQuery/jquery-3.6.0.min.js"></script>
        <script  id="jQueryUIThemeJs" src="<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/<?php echo $jQueryUITheme ?>/jquery-ui.min.js"></script>
        <script src="<?php echo env('APP_URL') ?>/resources/Bootstrap/bootstrap-5.0.2-dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script>
            $(function(){
                $('#txtDp').datepicker();
                $('#cboJQueryUITheme').val("<?php echo $jQueryUITheme ?>");
            });
            function jQueryThemeChanged(This){
                var jQueryUITheme = This.value;
                $.ajax({
                    url:'<?php echo env('APP_URL') ?>/public/jquery-ui-theme-changed',
                    type:'post',
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    data:JSON.stringify({ jQueryUITheme:jQueryUITheme }),
                    success:function(response){
                        var data=$.trim(response);
                        //alert(data);
                        if(data!=null){
                            $('#jQueryUIThemeCss').attr('href','<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/'+data+'/jquery-ui.min.css');
                            $('#jQueryUIThemeJs').attr('src','<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/'+data+'/jquery-ui.min.js');
                        }
                    }
                });
                
                    
            }
        </script>
        
    </body>
</html>
