<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/summernote/summernote-bs4.min.css">
  <link href="<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
  <!-- Styles -->
        <link rel="stylesheet" href="<?php echo env('APP_URL') ?>/resources/css/app.css?v=<?php echo filemtime(base_path().'/resources/css/app.css') ?>" />
        
        <?php 
            $jQueryUITheme=request()->cookie('jQueryUITheme');
        ?>
        
        <link id="jQueryUIThemeCss" href="<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/<?php echo $jQueryUITheme ?>/jquery-ui.min.css" rel="stylesheet"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <link href="<?php echo env('APP_URL') ?>/resources/Bootstrap/bootstrap-5.0.2-dist/bootstrap-icons.css" rel="stylesheet" type="text/css"/>
<!--        <link href="<?php echo env('APP_URL') ?>/resources/Bootstrap/bootstrap-5.0.2-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>-->
<!--        <link href="<?php echo env('APP_URL') ?>/resources/Whydo/whydo.css" rel="stylesheet" type="text/css"/>-->
            
        <script src="{{ asset('js/app.js') }}" defer></script>
        
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="<?php echo env('APP_URL') ?>/resources/img/Logo.logo" alt="ERP" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-controlsidebar-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?php echo env('APP_URL') ?>/resources/img/Logo.logo" alt="ERP" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Dot Qore Tec. ERP</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      @include('components.Logout')

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="<?php echo __('all.search') ?>" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                {{ __('all.upm-module') }}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
              <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo env('APP_URL') ?>/public/company-details" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('all.company-details') }}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo env('APP_URL') ?>/public/branches" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('all.branches') }}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo env('APP_URL') ?>/public/work-classes" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ __('all.WorkClasses') }}</p>
                </a>
              </li>
            </ul>
          </li>
          <?php
                        
            foreach ($appModules as $AppModule){
          
          ?>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                {{ __('all.'.strtolower($AppModule->name)) }}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          </li>
          <?php } ?>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">@yield('page_name')</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
              @yield('breadcrumb')
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @yield('page_content')
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2022 <a href="#">Dot Qore Technologies</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.2.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<script src="<?php echo env('APP_URL') ?>/resources/jQuery/jquery-3.6.0.min.js"></script>
<script  id="jQueryUIThemeJs" src="<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/<?php echo $jQueryUITheme ?>/jquery-ui.min.js"></script>
<script src="<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/jszip.min.js" type="text/javascript"></script>
<script src="<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/pdfmake.min.js" type="text/javascript"></script>
<script src="<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/vfs_fonts.js" type="text/javascript"></script>
<script src="<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/buttons.html5.min.js" type="text/javascript"></script>
<!-- jQuery 
<script src="<?php //echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4
<script src="<?php //echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/jquery-ui/jquery-ui.min.js"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/moment/moment.min.js"></script>
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<!--<script src="<?php echo env('APP_URL') ?>/resources/views/layouts/admin-lte/js/demo.js"></script>-->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
@yield('page_scripts_before_all')


<!--        <script src="<?php //echo env('APP_URL') ?>/resources/Bootstrap/bootstrap-5.0.2-dist/js/bootstrap.min.js" type="text/javascript"></script>-->
<script src="<?php echo env('APP_URL') ?>/resources/Bootstrap/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script src="<?php echo env('APP_URL') ?>/resources/jQuery/jquery.alphanum-master/jquery.alphanum.js" type="text/javascript"></script>
<!--        <script src="<?php //echo env('APP_URL') ?>/resources/Whydo/whydo.jszip.min.js" type="text/javascript"></script>
        <script src="<?php //echo env('APP_URL') ?>/resources/Whydo/whydo.all.min.js" type="text/javascript"></script>-->
        <script src="<?php echo env('APP_URL') ?>/resources/jQuery/TimePicker/wickedpicker.min.js" type="text/javascript"></script>
        @yield('page_scripts_before')
        <script src="<?php echo env('APP_URL') ?>/resources/js/common.js" type="text/javascript"></script>
        <script>
            var LeftMainMenuIsOpen=false;
            var LeftMenuIsLongerThanTheScrollable=false;
            var jQueryUITheme="<?php echo $jQueryUITheme ?>";
            $(function(){
                DisplayPage();
                BrowserWindowResized();
                setTimeout(function(){
                    BrowserWindowResized();
                    
                },100);
                $("#confirmDialog").dialog({  
                    width:275,
                    modal: true
                });
                $("#confirmDialog").dialog('close');
                window.onresize=function(){
                    setTimeout(function(){
                        BrowserWindowResized();
                    },100);
                };
                window.onscroll=function(){
                    if(LeftMenuIsLongerThanTheScrollable){
                        $('#MenuItemsContainer').css('marginTop',(-1*$(this).scrollTop())+'px');
                    }
                };
                $('.LeftMenuAccordion').accordion();
                AfterjQueryLoad();
                if(window.localStorage.getItem('LeftMainMenuIsOpen')=='Yes'){
                    ToggleLeftMenu(null);
                }
                $("#Scrollable input").focus(function(){
                    if($('.Icon').hasClass('bi-arrow-left')){
                        ToggleLeftMenu(null);
                    }
                });
                $('[data-toggle="tooltip"]').tooltip();
            });
            
            function DisplayPage(){
                $('body').css('display','unset');
            }
            function ToggleLeftMenu(This){
               if($('.Icon').hasClass('bi-arrow-right')){
                   
                   $('#LeftMainMenu').css('display','unset');
                   $('#LeftMainMenu').animate({
                        width: '274px'
                   },'fast',function(){
                        $('.Icon').removeClass('bi-arrow-right');
                        $('.Icon').addClass('bi-arrow-left');
                        setTimeout(function(){
                            AdjustLeftMainMenuOrScrollableAreaHeight();
                        },100);
                        
                   });
                   LeftMainMenuIsOpen=true;
                   window.localStorage.setItem('LeftMainMenuIsOpen','Yes');
               }else if($('.Icon').hasClass('bi-arrow-left')){
                   
                   $('#LeftMainMenu').animate({
                        width: '0'
                   },'fast', function(){
                        $('#LeftMainMenu').css('display','none');
                        $('.Icon').removeClass('bi-arrow-left');
                        $('.Icon').addClass('bi-arrow-right');
                        AdjustLeftMainMenuOrScrollableAreaHeight();
                   });
                   LeftMainMenuIsOpen=false;
                   window.localStorage.setItem('LeftMainMenuIsOpen','No');
               }
            }
            function AdjustLeftMainMenuOrScrollableAreaHeight(){
                var LeftMenuHeight=$('#MenuItemsContainer').css('height');
                var ScrollableHeight=$('#Scrollable').css('height');
                LeftMenuHeight=''+LeftMenuHeight;
                LeftMenuHeight=LeftMenuHeight.replace('px','');
                LeftMenuHeight=1*LeftMenuHeight;
                ScrollableHeight=''+ScrollableHeight;
                ScrollableHeight=ScrollableHeight.replace('px','');
                ScrollableHeight=1*ScrollableHeight;
                
                if(LeftMenuHeight>ScrollableHeight&&window.innerHeight<LeftMenuHeight){
                    LeftMenuIsLongerThanTheScrollable=true;
                    let MarginBottom=LeftMenuHeight-ScrollableHeight+120;
                    $('#Scrollable').css('marginBottom',MarginBottom+'px');
                }else if(ScrollableHeight>LeftMenuHeight&&window.innerHeight<ScrollableHeight){
                    LeftMenuIsLongerThanTheScrollable=false;
                    ScrollableHeight=Math.ceil(ScrollableHeight);
                    $('#LeftMainMenu').css('minHeight',ScrollableHeight+'px');

                }else if(window.innerHeight>LeftMenuHeight){
                    $('#LeftMainMenu').css('minHeight',(window.innerHeight-120)+'px'); 
                }
            }
            function BrowserWindowResized(){
                AdjustLeftMainMenuOrScrollableAreaHeight();
            }
            
            function GotojQueryUIChangePage(){
                window.location='<?php echo env('APP_URL') ?>/public/jquery-ui-theme-changer-demo';
            }
            function Logout(){
                $("#confirmDialog").dialog('open');
                $('#confirmDialog button.btn.btn-primary').click(function(){
                    $.ajax({
                        url:'<?php echo env('APP_URL') ?>/public/logout',
                        type:'post',
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        data:JSON.stringify({ "_token": "<?php echo csrf_token() ?>" }),
                        success:function(response){
                            let Res=$.trim(response);
                            if(Res=='Done'){
                                window.location="<?php echo env('APP_URL') ?>/public";
                            }
                        }
                    });
                });
                $("button.ui-dialog-titlebar-close").remove();
                $("#confirmDialog p").html('<?php echo __('all.AreYouSure') ?>,<br/> <?php echo __('all.Logout') ?> ?');
            }
            function CloseConfirmationDialog(){
                $("#confirmDialog").dialog('close');
            }
        </script>
        @yield('page_scripts_after')
    <div id="confirmDialog" title="ERP1-<?php echo __('all.company-details') ?>" style="display:none;">
        <div style="padding: 9px;text-align: center;">
            <p></p>
            <button style="margin-top: 9px;" class="btn btn-primary">{{ __('all.Yes') }}</button>
            <button style="margin-top: 9px;" class="btn btn-secondary" onclick="CloseConfirmationDialog();">{{ __('all.No') }}</button>
        </div>
    </div>
</body>
</html>
