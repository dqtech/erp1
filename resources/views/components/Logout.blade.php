<div class="user-panel mt-3 pb-3 mb-3 d-flex">
    
    <div class="image">
      <img src="<?php echo env('APP_URL') ?>/resources/img/Users/<?php echo Auth::user()->id.'.user' ?>" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info" style="width:100%;">
        <table style="width:100%;">
            <tr>
                <td style="width:100%;">
                    <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                </td>
                <td>
                    <i class="bi bi-x" onclick="Logout();" data-toggle="tooltip" title="{{ __('all.Logout') }}" style="display: inline-block;cursor: pointer;color: white;"></i>
                </td>
            </tr>
        </table>
     </div>
</div>