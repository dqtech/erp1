@extends('layouts.admin-lte')
@section('page_name', __('all.dashboard'))
@section('page_content')
<div style="padding:9px;">
    <input type="text" id="txtDp"/>
    <table><tr><td>
        <label>{{  __('all.jQueryUITheme') }}</label>
            <select class="cboJQueryUITheme" onchange="jQueryThemeChanged(this);">
                <option value="Base">Base</option>
                <option value="Black-Tie">Black-Tie</option>
                <option value="Blitzer">Blitzer</option>
                <option value="Cupertino">Cupertino</option>
                <option value="Dark-Hive">Dark-Hive</option>
                <option value="Dot-Luv">Dot-Luv</option>
                <option value="Eggplant">Eggplant</option>
                <option value="Excite-Bike">Excite-Bike</option>
                <option value="Flick">Flick</option>
                <option value="Hot-Sneaks">Hot-Sneaks</option>
                <option value="Humanity">Humanity</option>
                <option value="Le-Frog">Le-Frog</option>
                <option value="Mint-Choc">Mint-Choc</option>
                <option value="Overcast">Overcast</option>
                <option value="Pepper-Grinder">Pepper-Grinder</option>
                <option value="Redmond">Redmond</option>
                <option value="Smoothness">Smoothness</option>
                <option value="South-Street">South-Street</option>
                <option value="Start">Start</option>
                <option value="Sunny">Sunny</option>
                <option value="Swanky-Purse">Swanky-Purse</option>
                <option value="Trontastic">Trontastic</option>
                <option value="UI-Darkness">UI-Darkness</option>
                <option value="UI-Lightness">UI-Lightness</option>
                <option value="Vader">Vader</option>
            </select>
       </td></tr></table>
</div>
    

@stop

@section('page_scripts_before')
    <script>
        function AfterjQueryLoad(){
            $('#txtDp').datepicker();
            $('.cboJQueryUITheme').val(jQueryUITheme);
            $('.cboJQueryUITheme').selectmenu({
                change: function( event, ui ) {
                    console.log(ui);
                    $.ajax({
                    url:'<?php echo env('APP_URL') ?>/public/jquery-ui-theme-changed',
                    type:'post',
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    data:JSON.stringify({ jQueryUITheme:ui.item.value }),
                    success:function(response){
                        var data=$.trim(response);
                        //alert(data);
                        if(data!=null){
                            //$('.cboJQueryUITheme').val(data);
                            
                            $('#jQueryUIThemeCss').attr('href','<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/'+data+'/jquery-ui.min.css');
                            $('#jQueryUIThemeJs').attr('src','<?php echo env('APP_URL') ?>/resources/jQuery/jQueryUI/'+data+'/jquery-ui.min.js');
                        }
                    }
                });
                }
            });
        }
        
    </script>
@stop