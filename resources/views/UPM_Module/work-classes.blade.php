@extends('layouts.admin-lte')
@section('title', 'ERP1-'.__('all.WorkClasses'))
@section('page_name', __('all.WorkClasses'))
@section('breadcrumb')
<!--<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#">{{ __('all.branches') }}</a></li>
</ol>-->
@stop
@section('page_content')

<div style="padding-top: 15px;">
    
    <div style="padding: 15px;">
        <button class="btn btn-success" style="margin-bottom: 18px;" onclick="DirectToCreateNewWorkClassPage();">{{ __('all.create-new-work-class') }}</button>
        
        
        <table id="jqdtWorkClasses">
            <thead>
                <tr>
                    <th>{{ __('all.app-module') }}</th>
                    <th>{{ __('all.code') }}</th>
                    <th>{{ __('all.WorkClass') }}</th>
                    <th>{{ __('all.start') }}</th>
                    <th>{{ __('all.end') }}</th>
                    <th>{{ __('all.active') }}</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    //xdebug_break();
                    if($workClasses!=null){
                ?>
                    <?php foreach($workClasses as $workClass){ ?>
    <tr>
        <?php 
            //xdebug_break();
        ?>
        <td>{{ $workClass['app_module'] }}</td>
        <td>{{ $workClass['code'] }}</td>
        <td>{{ $workClass['name'] }}</td>
        <td>{{ $workClass['start'] }}</td>
        <td>{{ $workClass['end'] }}</td>
        <td><label style="color:<?php echo (($workClass['active']=='Active')?'green':'red') ?>;">{{ $workClass['active'] }}</label></td>
        <td style="text-align: right;">
            <input type="image" data-toggle="tooltip" title="{{ __('all.view-work-class') }}" src="<?php echo env('APP_URL') ?>/resources/img/View.png" class="AddViewEditDeleteIcons" onclick="DirectToViewWorkClassPage(<?php echo $workClass['id'] ?>);"/>
            <input type="image" data-toggle="tooltip" title="{{ __('all.edit-work-class') }}" src="<?php echo env('APP_URL') ?>/resources/img/Edit.png" class="AddViewEditDeleteIcons" onclick="DirectToEditWorkClassPage(<?php echo $workClass['id'] ?>);"/>
            <input type="image" data-toggle="tooltip" title="{{ __('all.delete-work-class') }}" src="<?php echo env('APP_URL') ?>/resources/img/Delete.png"  class="AddViewEditDeleteIcons" onclick="DirectToDeleteWorkClassPage(<?php echo $workClass['id'] ?>);"/>
        </td>
    </tr>
                    <?php } ?>
                <?php }else{ ?>

                <?php } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>{{ __('all.app-module') }}</td>
                    <td>{{ __('all.code') }}</td>
                    <td>{{ __('all.WorkClass') }}</td>
                    <td>{{ __('all.start') }}</td>
                    <td>{{ __('all.end') }}</td>
                    <td>{{ __('all.active') }}</td>
                    <td>&nbsp;</td>
                </tr>
            </tfoot>
        </table>
        
    </div>
</div>
@stop
@section('page_scripts_before')
<script>
function AfterjQueryLoad(){
    $("#successDialog").dialog({  
        width:275,
        modal: true
    });
    
    $("#successDialog").dialog('close');
    let v_value=(new Date()).getMilliseconds();
    $('#jqdtWorkClasses').DataTable({
        language:{
            url:'<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/<?php echo Config::get('app.locale') ?>.json?v='+v_value
        },
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columnDefs: [
            { width: 300 },
            { width: 90 },
            { width: 300 },
            { width: 80 },
            { width: 80 },
            { width: 100 },
            { width: 90 }
        ],
        fixedColumns: false
    });
}

</script>
@stop
@section('page_scripts_after')
<script>
function ValidToPost(){
    return true;
}
function displaySuccess(){
    $('#successDialog').css('display','unset');
    $("#successDialog").dialog("open");
    $("button.ui-dialog-titlebar-close").remove();
}
function CloseDialog(){
    $("#successDialog").dialog("close");
}
function EmailChanged(){
    let Email=$('#txtEmail').val();
    Email=$.trim(Email);
    if(Email!=''){
        if(EmailIsValid(Email)){
            $('#imgEmailError').css('display','none');
        }else{
            $('#imgEmailError').css('display','inline-block');
            $('#imgEmailError').attr('title','<?php echo __('all.email-format-invalid') ?>');
        }
    }else{
        $('#imgEmailError').css('display','none');
    }
}
function New(){
    $('.Input input').val('');
    $('#hdnBranchID').val('0');
}
function DirectToCreateNewWorkClassPage(){
    window.location = '<?php echo env('APP_URL') ?>/public/create-new-work-class';
}
function DirectToViewWorkClassPage(id){
    window.location = '<?php echo env('APP_URL') ?>/public/view-work-class?id='+id;
}
function DirectToEditWorkClassPage(id){
    window.location = '<?php echo env('APP_URL') ?>/public/edit-work-class?id='+id;
}
function DirectToDeleteWorkClassPage(id){
    window.location = '<?php echo env('APP_URL') ?>/public/delete-work-class?id='+id;
}
</script>
@stop