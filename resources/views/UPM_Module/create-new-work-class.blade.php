@extends('layouts.admin-lte')
@section('title', 'ERP1-'.__('all.create-new-work-class'))
@section('page_name', __('all.create-new-work-class'))
@section('breadcrumb')
<!--<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#">{{ __('all.company-details') }}</a></li>
</ol>-->
@stop
@section('page_content')

<div style="padding-top: 15px;">
    
    <div style="padding: 15px;">
        <form action="<?php echo env('APP_URL') ?>/public/save-work-class" method="post" target="ifrmTarget">
            {{ csrf_field() }}
            <input type="hidden" id="hdnWorkClassID" name="id" value="0"/>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.app-module') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <select id="cboAppModules" name="app_module" onchange="AppModuleChanged();">
                                    <?php 
                                          $firstAppModule=$appModules[0];
                                          foreach($appModules as $appModule){ 
                                    ?>
                                            <option value="<?php echo $appModule->id ?>">{{ $appModule->name }}</option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" id="hdnAppModuleID" name="app_module_id" value="<?php echo $firstAppModule->id ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.code') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="text" maxlength="10" id="txtCode" name="code" value="" style="width:100%;" class="CodeInput"  onkeyup="CodeChanged();" onclick="CodeChanged();" onblur="CodeChanged();" onfocusout="CodeChanged();" />
                            </td>
                            <td style="max-width: 30px;">
                                <img id="imgCodeRequired" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip" title="<?php echo __('all.required') ?>"/>
                                <img id="imgCodeExists" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip" title="<?php echo __('all.exists') ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.WorkClass') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <input type="text" maxlength="150" id="txtName" name="name" value="" style="width:100%;" class="NameInput"  onkeyup="NameChanged();" onclick="NameChanged();" onblur="NameChanged();" onfocusout="NameChanged();"/>                                
                            </td>
                            <td style="max-width: 30px;">
                                <img id="imgNameRequired" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip" title="<?php echo __('all.required') ?>"/>
                                <img id="imgNameExists" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip" title="<?php echo __('all.exists') ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.start').' '.__('all.time') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="hidden" id="hdnStart" name="start"/>
                                <input type="text" maxlength="2" id="txtStartHour" onchange="StartHourChanged();"  value="" class="HourInput TimeInput"/>:
                                <input type="text" maxlength="2" id="txtStartMinute" onchange="StartMinuteChanged();" value="" class="MinuteInput TimeInput"/>
                            </td>
                            <td style="max-width: 30px;">
                                <img id="imgStartTimeError" src="<?php echo env('APP_URL') ?>/resources/img/Warning.png" style="display:none;width:30px;height: 30px;cursor: pointer;" title=""/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.end').' '.__('all.time') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="hidden" id="hdnEnd" name="end"/>
                                <input type="text" maxlength="2" id="txtEndHour"  value=""  onchange="EndHourChanged();" class="HourInput TimeInput"/>:
                                <input type="text" maxlength="2" id="txtEndMinute"  value="" onchange="EndMinuteChanged();" class="MinuteInput TimeInput"/>
                            </td>
                            <td style="max-width: 30px;">
                                <img id="imgEndTimeError" src="<?php echo env('APP_URL') ?>/resources/img/Warning.png" style="display:none;width:30px;height: 30px;cursor: pointer;" title=""/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.active') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 CheckBox">
                    <input type="checkbox" id="chkActive" name="active" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ButtonInput">
                    <input class="btn btn-primary" style="margin-right: 7px;" type="button" value="{{ __('all.back-to-list') }}" onclick="DirectToListPage();"/><button type="submit" onclick="return ValidToPost();" class="btn btn-danger" style="margin-right: 7px;">{{ __('all.save') }}</button><input class="btn btn-secondary" style="margin-left: 7px;" type="button" value="{{ __('all.refresh') }}" onclick="Refresh();"/>
                </div>
                
            </div>
        </form>
    </div>
    <iframe style="display: none;" id="ifrmTarget" name="ifrmTarget"></iframe>
    <div id="successDialog" title="ERP1-<?php echo __('all.company-details') ?>" style="display:none;">
        <div style="padding: 9px;text-align: center;">
            <p>{{ __('all.successfully-saved') }}</p>
            <button style="margin-top: 9px;" class="btn btn-success" onclick="CloseDialog();">{{ __('all.ok') }}</button>
        </div>
    </div>
</div>
@stop
@section('page_scripts_before')
<script>
function AfterjQueryLoad(){
    
    $("#successDialog").dialog({  
        width:275,
        modal: true
    });
    $("#successDialog").dialog('close');
    
    $('.HourInput').numeric({
        allowMinus   : false,
        allowThouSep : false,
        allowDecSep : false,
        max:24,
        min:0
    });
    
    $('.MinuteInput').numeric({
        allowMinus   : false,
        allowThouSep : false,
        allowDecSep : false,
        max:59,
        min:0
    });
    
    $('.TimeInput').focusout(function(){
        let Value=this.value;
        while(Value.length<2){
            Value='0'+Value;
            
        }
        this.value=Value;
    });
    
    $('.TimeInput').blur(function(){
        let Value=this.value;
        while(Value.length<2){
            Value='0'+Value;
            
        }
        this.value=Value;
    });
    
    
}

</script>
@stop
@section('page_scripts_after')
<script>

function ValidToPost(){
    return true;
}
function displaySuccess(){
    $('#successDialog').css('display','unset');
    $("#successDialog").dialog("open");
    $("button.ui-dialog-titlebar-close").remove();
}
function CloseDialog(){
    $("#successDialog").dialog("close");
}
function CodeChanged(){
    let Code=$('#txtCode').val();
    Code=$.trim(Code);
    $('#imgCodeRequired').css('display','none');
    $('#imgCodeExists').css('display','none');
    if(Code==''){
        $('#imgCodeRequired').css('display','inline-block');
    }else{
        $.ajax({
            url:'<?php echo env('APP_URL') ?>/public/work-class-code-exists',
            headers: { 'X-CSRF-TOKEN': '<?php echo csrf_token() ?>' },
            type:'post',
            contentType: "application/json; charset=utf-8",
            dataType: "text",
            data:JSON.stringify({ 
                app_module_id:$('#cboAppModules').val(),
                code:Code
            }),
            success:function(response){
                let data=$.trim(response);
                //alert(data);
                if(data=='Yes'){
                    $('#imgCodeExists').css('display','inline-block');
                    //$('#imgCodeExists').attr('title','<?php //echo __('all.code').' '.__('all.exists') ?>');
                }else if(data=='No'){
                    $('#imgCodeExists').css('display','none');
                }
            }
        });
    }
}
function NameChanged(){
    let Name=$('#txtName').val();
    Name=$.trim(Name);
    $('#imgNameRequired').css('display','none');
    $('#imgNameExists').css('display','none');
    if(Name==''){
        $('#imgNameRequired').css('display','inline-block');
    }else{
        $.ajax({
            url:'<?php echo env('APP_URL') ?>/public/work-class-name-exists',
            headers: { 'X-CSRF-TOKEN': '<?php echo csrf_token() ?>' },
            type:'post',
            contentType: "application/json; charset=utf-8",
            dataType: "text",
            data:JSON.stringify({ 
                app_module_id:$('#cboAppModules').val(),
                name:Name
            }),
            success:function(response){
                let data=$.trim(response);
                //alert(data);
                if(data=='Yes'){
                    $('#imgNameExists').css('display','inline-block');
                    //$('#imgNameExists').attr('title','<?php //echo __('all.name').' '.__('all.exists') ?>');
                }else if(data=='No'){
                    $('#imgNameExists').css('display','none');
                }
            }
        });
    }
}
function StartHourChanged(){
    setTimeout(function(){
        let Hour=$('#txtStartHour').val();
        let Minute=$('#txtStartMinute').val();
        if((Hour.length==2)&&(Minute.length==2)){
            $('#hdnStart').val(Hour+':'+Minute);
        }else{
            $('#hdnStart').val('');
        }
    },10);
    
}
function StartMinuteChanged(){
    
    setTimeout(function(){
        let Minute=$('#txtStartMinute').val();
        let Hour=$('#txtStartHour').val();
        if((Hour.length==2)&&(Minute.length==2)){
            $('#hdnStart').val(Hour+':'+Minute);
        }else{
            $('#hdnStart').val('');
        }
    },10);
    
}
function EndHourChanged(){
    setTimeout(function(){
        let Hour=$('#txtEndHour').val();
        let Minute=$('#txtEndMinute').val();
        if((Hour.length==2)&&(Minute.length==2)){
            $('#hdnEnd').val(Hour+':'+Minute);
        }else{
            $('#hdnEnd').val('');
        }
    },10);
    
}
function EndMinuteChanged(){
     setTimeout(function(){
        let Minute=$('#txtEndMinute').val();
        let Hour=$('#txtEndHour').val();
        if((Hour.length==2)&&(Minute.length==2)){
            $('#hdnEnd').val(Hour+':'+Minute);
        }else{
            $('#hdnEnd').val('');
        }
    },10);

}
function DirectToListPage(){
    window.location = '<?php echo env('APP_URL') ?>/public/work-classes';
}
function AppModuleChanged(){
    let AppModuleID=$('#cboAppModules').val();
    $('#hdnAppModuleID').val(AppModuleID);
}
</script>
@stop