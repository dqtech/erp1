@extends('layouts.admin-lte')
@section('title', 'ERP1-'.__('all.company-details'))
@section('page_name', __('all.company-details'))
@section('breadcrumb')
<!--<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#">{{ __('all.company-details') }}</a></li>
</ol>-->
@stop
@section('page_content')
<?php 
    $companyName='';
    $address='';
    $vat_reg_no='';
    $vat_percentage='';
    $phone='';
    $fax='';
    $email='';
    $active=false;
    if($companyDetails!=null){
        $companyName=$companyDetails->name;
        $address=$companyDetails->address;
        $vat_reg_no=$companyDetails->vat_reg_no;
        //xdebug_break();
        $vat_percentage=$companyDetails->vat_percentage;
        $phone=$companyDetails->phone;
        $fax=$companyDetails->fax;
        $email=$companyDetails->email;
        $active=$companyDetails->active;
    }
?>
<div style="padding-top: 15px;">
    
    <div style="padding: 15px;">
        <form action="<?php echo env('APP_URL') ?>/public/save-company-details" method="post" target="ifrmTarget">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.company-name') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <input type="text" maxlength="150" id="txtName" name="name" value="<?php echo $companyName ?>" style="width:100%;" class="NameInput"/>                                
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.address') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <textarea class="AddressInput" maxlength="500" cols="100" rows="5" style="resize: none;width:100%;" id="txtAddress" name="address">{{ $address }}</textarea>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.vat-registration-no') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="text" maxlength="36" class="RegNoInput" id="txtVatRegNo" name="vat_reg_no" style="width:100%;" value="<?php echo $vat_reg_no ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.vat-percentage') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="text" maxlength="5" id="txtVatPercentage" name="vat_percentage" class="FloatInput"  value="<?php echo $vat_percentage ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.phone') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="text" maxlength="15" id="txtPhone" name="phone" class="PhoneNoInput"  value="<?php echo $phone ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.fax') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="text" maxlength="15" id="txtFax" name="fax" class="PhoneNoInput" value="<?php echo $fax ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.email') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:100%;"><input type="text" maxlength="150" id="txtEmail" name="email" onkeyup="EmailChanged();" onclick="EmailChanged();" class="EmailInput"  style="width:100%;"  value="<?php echo $email ?>"/></td>
                            <td style="max-width: 30px;">
                                <img id="imgEmailError" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" title=""/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.active') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 CheckBox">
                    <input type="checkbox" id="chkActive" name="active" <?php echo (($active)?'checked="checked"':'') ?> />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 ButtonInput">
                    <button type="submit" onclick="return ValidToPost();" class="btn btn-danger" style="margin-right: 7px;">{{ __('all.save') }}</button><input class="btn btn-secondary" style="margin-left: 7px;" type="button" value="{{ __('all.refresh') }}" onclick="Refresh();"/>
                </div>
                
            </div>
        </form>
    </div>
    <iframe style="display: none;" id="ifrmTarget" name="ifrmTarget"></iframe>
    <div id="successDialog" title="ERP1-<?php echo __('all.company-details') ?>" style="display:none;">
        <div style="padding: 9px;text-align: center;">
            <p>{{ __('all.successfully-saved') }}</p>
            <button style="margin-top: 9px;" class="btn btn-success" onclick="CloseDialog();">{{ __('all.ok') }}</button>
        </div>
    </div>
</div>
@stop
@section('page_scripts_before')
<script>
function AfterjQueryLoad(){
    $(".FloatInput").numeric({
        allowMinus   : false,
        allowThouSep : false
    });
    $("#successDialog").dialog({  
        width:275,
        modal: true
    });
    $("#successDialog").dialog('close');
}

</script>
@stop
@section('page_scripts_after')
<script>
function Refresh(){
    var v_value=(new Date()).getMilliseconds();
    //alert(v);
    $.ajax({
        url:'<?php echo env('APP_URL') ?>/public/get-company-details?v='+v_value,
        type:'get',
        contentType: "application/json; charset=utf-8",
        dataType: "text",
        success:function(response){
            var data=$.trim(response);
            //alert(data);
            if(data!=null){
                if(data!=''){
                    let companyDetails=JSON.parse(data);
                    $('#imgEmailError').css('display','none');
                    //alert(companyDetails.name);
                    $('#txtName').val(companyDetails.name);
                    $('#txtAddress').val(companyDetails.address);
                    $('#txtVatRegNo').val(companyDetails.vat_reg_no);
                    $('#txtVatPercentage').val(companyDetails.vat_percentage);
                    $('#txtPhone').val(companyDetails.phone);
                    $('#txtFax').val(companyDetails.fax);
                    $('#txtEmail').val(companyDetails.email);
                    if(companyDetails.active){
                        $('#chkActive').prop('checked',true);
                    }else{
                        $('#chkActive').prop('checked',false);
                    }
                }
            }
        }
    });
}
function ValidToPost(){
    return true;
}
function displaySuccess(){
    $('#successDialog').css('display','unset');
    $("#successDialog").dialog("open");
    $("button.ui-dialog-titlebar-close").remove();
}
function CloseDialog(){
    $("#successDialog").dialog("close");
}
function EmailChanged(){
    let Email=$('#txtEmail').val();
    Email=$.trim(Email);
    if(Email!=''){
        if(EmailIsValid(Email)){
            $('#imgEmailError').css('display','none');
        }else{
            $('#imgEmailError').css('display','inline-block');
            $('#imgEmailError').attr('title','<?php echo __('all.email-format-invalid') ?>')
        }
    }else{
        $('#imgEmailError').css('display','none');
    }
}
</script>
@stop