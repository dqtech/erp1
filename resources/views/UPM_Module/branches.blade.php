@extends('layouts.admin-lte')
@section('title', 'ERP1-'.__('all.branches'))
@section('page_name', __('all.branches'))
@section('breadcrumb')
<!--<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#">{{ __('all.branches') }}</a></li>
</ol>-->
@stop
@section('page_content')

<div style="padding-top: 15px;">
    
    <div style="padding: 15px;">
        <button class="btn btn-success" style="margin-bottom: 18px;" onclick="DirectToCreateNewBranchPage();">{{ __('all.create-new-branch') }}</button>
        <?php 
            if($branches!=null){
        ?>
        
        <table id="jqdtBranches">
            <thead>
                <tr>
                    <th>{{ __('all.code') }}</th>
                    <th>{{ __('all.branch-name') }}</th>
                    <th>{{ __('all.phone') }} #1</th>
                    <th>{{ __('all.phone') }} #2</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($branches as $branch){ ?>
                <tr>
                    <?php 
                        //xdebug_break();
                    ?>
                    <td>{{ $branch->code }}</td>
                    <td>{{ $branch->name }}</td>
                    <td>{{ $branch->phone1 }}</td>
                    <td>{{ $branch->phone2 }}</td>
                    <td style="text-align: right;">
                        <input type="image" data-toggle="tooltip" title="{{ __('all.view-branch') }}" src="<?php echo env('APP_URL') ?>/resources/img/View.png" class="AddViewEditDeleteIcons" onclick="DirectToViewBranchPage(<?php echo $branch->id ?>);"/>
                        <input type="image" data-toggle="tooltip" title="{{ __('all.edit-branch') }}" src="<?php echo env('APP_URL') ?>/resources/img/Edit.png" class="AddViewEditDeleteIcons" onclick="DirectToEditBranchPage(<?php echo $branch->id ?>);"/>
<!--                        <input type="image" title="{{ __('all.delete-branch') }}" src="<?php //echo env('APP_URL') ?>/resources/img/Delete.png"  class="AddViewEditDeleteIcons" onclick="DirectToDeleteBranchPage(<?php echo $branch->id ?>);"/>-->
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>{{ __('all.code') }}</td>
                    <td>{{ __('all.branch-name') }}</td>
                    <td>{{ __('all.phone') }} #1</td>
                    <td>{{ __('all.phone') }} #2</td>
                    <td>&nbsp;</td>
                </tr>
            </tfoot>
        </table>
        <?php }else{ ?>
            
        <?php } ?>
    </div>
</div>
@stop
@section('page_scripts_before')
<script>
function AfterjQueryLoad(){
    $("#successDialog").dialog({  
        width:275,
        modal: true
    });
    
    $("#successDialog").dialog('close');
    let v_value=(new Date()).getMilliseconds();
    $('#jqdtBranches').DataTable({
        language:{
            url:'<?php echo env('APP_URL') ?>/resources/jQuery/DataTables/<?php echo Config::get('app.locale') ?>.json?v='+v_value
        },
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columnDefs: [
            { width: 80, targets: 0 },
            { width: 650, targets: 0 },
            { width: 100, targets: 0 },
            { width: 100, targets: 0 },
            { width: 90, targets: 0 }
        ],
        fixedColumns: false
    });
}

</script>
@stop
@section('page_scripts_after')
<script>
function ValidToPost(){
    return true;
}
function displaySuccess(){
    $('#successDialog').css('display','unset');
    $("#successDialog").dialog("open");
    $("button.ui-dialog-titlebar-close").remove();
}
function CloseDialog(){
    $("#successDialog").dialog("close");
}
function EmailChanged(){
    let Email=$('#txtEmail').val();
    Email=$.trim(Email);
    if(Email!=''){
        if(EmailIsValid(Email)){
            $('#imgEmailError').css('display','none');
        }else{
            $('#imgEmailError').css('display','inline-block');
            $('#imgEmailError').attr('title','<?php echo __('all.email-format-invalid') ?>');
        }
    }else{
        $('#imgEmailError').css('display','none');
    }
}
function New(){
    $('.Input input').val('');
    $('#hdnBranchID').val('0');
}
function DirectToCreateNewBranchPage(){
    window.location = '<?php echo env('APP_URL') ?>/public/create-new-branch';
}
function DirectToViewBranchPage(id){
    window.location = '<?php echo env('APP_URL') ?>/public/view-branch?id='+id;
}
function DirectToEditBranchPage(id){
    window.location = '<?php echo env('APP_URL') ?>/public/edit-branch?id='+id;
}
function DirectToDeleteBranchPage(id){
    window.location = '<?php echo env('APP_URL') ?>/public/delete-branch?id='+id;
}
</script>
@stop