@extends('layouts.admin-lte')
@section('title', 'ERP1-'.__('all.edit-branch'))
@section('page_name', __('all.edit-branch'))
@section('breadcrumb')
<!--<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#">{{ __('all.branches') }}</a></li>
</ol>-->
@stop
@section('page_content')
<?php  
    $id=0;
    $code='';
    $name='';
    $address='';
    $phone1='';
    $phone2='';
    $fax='';
    $email='';
    $active=false;
    if($branch!=null){
        $id=$branch->id;
        $code=$branch->code;
        $name=$branch->name;
        $address=$branch->address;
        $phone1=$branch->phone1;
        $phone2=$branch->phone2;
        $fax=$branch->fax;
        $email=$branch->email;
        $active=$branch->active;
    }
?>
<div style="padding-top: 15px;">
    
    <div style="padding: 15px;">
        <form action="<?php echo env('APP_URL') ?>/public/save-branch" method="post" target="ifrmTarget">
            {{ csrf_field() }}
            <input type="hidden" id="hdnBranchID" name="id" value="<?php echo $id ?>"/>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.branch-code') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="hidden" id="hdnCode" value="<?php echo $code ?>"/>
                                <input onkeyup="CodeChanged();" onclick="CodeChanged();" onblur="CodeChanged();" onfocusout="CodeChanged();" type="text" maxlength="10" id="txtCode" name="code" value="<?php echo $code ?>" style="width:100%;" class="NameInput"/>
                            </td>
                            <td style="max-width: 30px;">
                                <img id="imgCodeRequired" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip" title="<?php echo __('all.required') ?>"/>
                                <img id="imgCodeExists" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip" title="<?php echo __('all.exists') ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.branch-name') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <input type="hidden" id="hdnName" value="<?php echo $name ?>"/>
                                <input  onkeyup="NameChanged();" onclick="NameChanged();" onblur="NameChanged();" onfocusout="NameChanged();"   type="text" maxlength="150" id="txtName" name="name" value="<?php echo $name ?>" style="width:100%;" class="NameInput"/>
                            </td>
                            <td style="max-width: 30px;">
                                <img id="imgNameRequired" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip" title="<?php echo __('all.required') ?>"/>
                                <img id="imgNameExists" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip" title="<?php echo __('all.exists') ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.address') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <textarea class="AddressInput" maxlength="500" cols="100" rows="5" style="resize: none;width:100%;" id="txtAddress" name="address">{{ $address }}</textarea>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.phone') }} #1</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="text" maxlength="15" id="txtPhone1" name="phone1" class="FloatInput"  value="<?php echo $phone1 ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.phone') }} #2</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="text" maxlength="15" id="txtPhone2" name="phone2" class="FloatInput"  value="<?php echo $phone2 ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.fax') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table>
                        <tr>
                            <td>
                                <input type="text" maxlength="15" id="txtFax" name="fax" class="PhoneNoInput" value="<?php echo $fax ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.email') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 Input">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:100%;">
                                <input type="text" maxlength="150" id="txtEmail" name="email" onkeyup="EmailChanged();" onclick="EmailChanged();" onblur="EmailChanged();" onfocusout="EmailChanged();" class="EmailInput"  style="width:100%;"  value="{{ $email }}"/>
                                <input type="hidden" id="hdnEmail"   value="{{ $email }}"/>
                            </td>
                            <td style="max-width: 30px;">
                                <img id="imgEmailError" src="<?php echo env('APP_URL') ?>/resources/img/error.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip" title="<?php echo __('all.email-format-invalid') ?>"/>
                                <img id="imgEmailExists" src="<?php echo env('APP_URL') ?>/resources/img/Warning.png" style="display:none;width:30px;height: 30px;cursor: pointer;" data-toggle="tooltip"  title="<?php echo __('all.email-exists') ?>"/>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 Label">
                    <label style="display:inline-block;">{{ __('all.active') }}</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 CheckBox">
                    <input type="checkbox" id="chkActive" name="active" <?php echo $active==true?'checked="checked"':''; ?>/>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12 ButtonInput">
                    <input class="btn btn-primary" style="margin-right: 7px;" type="button" value="{{ __('all.back-to-list') }}" onclick="DirectToListPage();"/><button type="submit" onclick="return ValidToPost();" class="btn btn-danger" style="margin-right: 7px;">{{ __('all.save') }}</button><input class="btn btn-secondary" style="margin-left: 7px;" type="button" value="{{ __('all.refresh') }}" onclick="Refresh();"/>
                </div>
                
            </div>
        </form>
    </div>
    <iframe style="display: none;" id="ifrmTarget" name="ifrmTarget"></iframe>
    <div id="successDialog" title="ERP1-<?php echo __('all.branches') ?>" style="display:none;">
        <div style="padding: 9px;text-align: center;">
            <p>{{ __('all.successfully-saved') }}</p>
            <button style="margin-top: 9px;" class="btn btn-success" onclick="CloseDialog();">{{ __('all.ok') }}</button>
        </div>
    </div>
</div>
@stop
@section('page_scripts_before')
<script>
function AfterjQueryLoad(){
    $("#successDialog").dialog({  
        width:275,
        modal: true
    });
    $("#successDialog").dialog('close');
    $('[data-toggle="tooltip"]').tooltip();
}

</script>
@stop
@section('page_scripts_after')
<script>
function Refresh(){
    var v_value=(new Date()).getMilliseconds();
    //alert(v);
    $.ajax({
        url:'<?php echo env('APP_URL') ?>/public/get-branch-details?v='+v_value+'&id='+$('hdnBranchID').val(),
        type:'get',
        contentType: "application/json; charset=utf-8",
        dataType: "text",
        success:function(response){
            var data=$.trim(response);
            //alert(data);
            if(data!=null){
                if(data!=''){
                    let branchDetails=JSON.parse(data);
                    //alert(companyDetails.name);
                    $('#txtName').val(branchDetails.name);
                    $('#txtAddress').val(branchDetails.address);
                    $('#txtPhone1').val(branchDetails.phone1);
                    $('#txtPhone2').val(branchDetails.phone2);
                    $('#txtFax').val(branchDetails.fax);
                    $('#txtEmail').val(branchDetails.email);
                    if(branchDetails.active){
                        $('#chkActive').prop('checked',true);
                    }else{
                        $('#chkActive').prop('checked',false);
                    }
                }
            }
        }
    });
}
function ValidToPost(){
    return true;
}
function displaySuccess(){
    $('#successDialog').css('display','unset');
    $("#successDialog").dialog("open");
    $("button.ui-dialog-titlebar-close").remove();
}
function CloseDialog(){
    $("#successDialog").dialog("close");
}
function EmailChanged(){
    let Email=$('#txtEmail').val();
    Email=$.trim(Email);
    $('#imgEmailError').css('display','none');
    $('#imgEmailExists').css('display','none');
    if(Email!=''){
        if(EmailIsValid(Email)){
            $('#imgEmailError').css('display','none');
            if(Email!==$('#hdnEmail').val()){
                $.ajax({
                    url:'<?php echo env('APP_URL') ?>/public/branch-email-exists',
                    headers: { 'X-CSRF-TOKEN': '<?php echo csrf_token() ?>' },
                    type:'post',
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    data:JSON.stringify({ email:Email }),
                    success:function(response){
                        let data=$.trim(response);
                        //alert(data);
                        if(data=='Yes'){
                            $('#imgEmailExists').css('display','inline-block');
                            $('#imgEmailExists').attr('title','<?php echo __('all.email-exists') ?>');
                        }else if(data=='No'){
                            $('#imgEmailExists').css('display','none');
                        }
                    }
                });
            }else{
                $('#imgEmailExists').css('display','none');
            }
            
        }else{
            $('#imgEmailError').css('display','inline-block');
            $('#imgEmailError').attr('title','<?php echo __('all.email-format-invalid') ?>');
        }
    }else{
        $('#imgEmailError').css('display','none');
    }
}
function DirectToListPage(){
    window.location = '<?php echo env('APP_URL') ?>/public/branches';
}
function CodeChanged(){
    let Code=$('#txtCode').val();
    Code=$.trim(Code);
    $('#imgCodeRequired').css('display','none');
    $('#imgCodeExists').css('display','none');
    if(Code==''){
        $('#imgCodeRequired').css('display','inline-block');
    }else if(Code!=$('#hdnCode').val()){
        $.ajax({
            url:'<?php echo env('APP_URL') ?>/public/branch-code-exists',
            headers: { 'X-CSRF-TOKEN': '<?php echo csrf_token() ?>' },
            type:'post',
            contentType: "application/json; charset=utf-8",
            dataType: "text",
            data:JSON.stringify({ 
                code:Code
            }),
            success:function(response){
                let data=$.trim(response);
                //alert(data);
                if(data=='Yes'){
                    $('#imgCodeExists').css('display','inline-block');
                    //$('#imgCodeExists').attr('title','<?php //echo __('all.code').' '.__('all.exists') ?>');
                }else if(data=='No'){
                    $('#imgCodeExists').css('display','none');
                }
            }
        });
    }
}
function NameChanged(){
    let Name=$('#txtName').val();
    Name=$.trim(Name);
    $('#imgNameRequired').css('display','none');
    $('#imgNameExists').css('display','none');
    if(Name==''){
        $('#imgNameRequired').css('display','inline-block');
    }else if(Code!=$('#hdnName').val()){
        $.ajax({
            url:'<?php echo env('APP_URL') ?>/public/branch-name-exists',
            headers: { 'X-CSRF-TOKEN': '<?php echo csrf_token() ?>' },
            type:'post',
            contentType: "application/json; charset=utf-8",
            dataType: "text",
            data:JSON.stringify({ 
                name:Name
            }),
            success:function(response){
                let data=$.trim(response);
                //alert(data);
                if(data=='Yes'){
                    $('#imgNameExists').css('display','inline-block');
                    //$('#imgNameExists').attr('title','<?php //echo __('all.name').' '.__('all.exists') ?>');
                }else if(data=='No'){
                    $('#imgNameExists').css('display','none');
                }
            }
        });
    }
}

</script>
@stop